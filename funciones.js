//Funciones

/*
console.log('Hola Mundo');
prompt('Cuantos años tienes?');
alert('Error');
*/

//Function Declaration
function saludar(nombre) {
    console.log(`Hola ${nombre}`);
}
saludar('Leonel'); //llama a la función enviando el parametro 'Leonel'
saludar('Jorge');

function sumar(a, b) {    //estas funciones solo imprimen un valor
    console.log(a + b);
}
sumar(1, 2);
sumar(3,4);

//Funciones que retornan un valor y se guarda en una variable

function restar(a,b) {
    return a - b;
}
let resta;

resta = restar(5,2);
resta = restar(543,20);
resta = restar(10,5);

console.log(resta);


// condición de undefined
function saludar1(name) {
    return `Hola ${name}`;
}
let saludo;

saludo = saludar1('Leonel Soria');
console.log(saludo);

function saludar2(nombre1 = 'Visitante') {
   /*En caso de que no se envie un parámetro en el llamado de la función
   imprimirá 'Visitante' por default*/
    return `Hola ${nombre1}`;
}
let saludo1;
saludo1 = saludar2('Carlox');
console.log(saludo1);

//Function Expression

const suma2 = function(a, b) {
    return a + b;
}
console.log(suma2(1, 2)); 


const saludarExpress = function(nombreExpress = 'Visitante') {
    /*En caso de que no se envie un parámetro en el llamado de la función
   imprimirá 'Visitante' por default*/
    return `hola ${nombreExpress}`
}
console.log(saludarExpress('Leo'));

//IIFE son funciones que se declaran y se invocan inmediatamente

(function(tecnologia) {
    console.log(`Aprendiendo ${tecnologia}`);
}) ('JavaScript');

/*Metodos de propiedad - cuando una función se pone dentro de un objeto*/
const musica = {
    reproducir: function(id) {
        console.log(`Reproduciendo canción: ${id}`);
    },
    pausar: function() {
        console.log('Pausa');
    }
}
//Los metodos tambien pueden cambiarse o crearse fuera del objeto
musica.borrar = function(id) {
    console.log(`Borrando la cancion ${id}`);
}

musica.reproducir(30);
musica.pausar();
musica.borrar(2);

//Funcion que no existe
try {       //Try intenta ejecutar la función
    algo();
} catch (error) {  //Catch
    console.log(error);
} finally {   //Aunque haya un error o no, Finally siempre se ejecuta
    console.log('No le importa, ejecuta de todos modos');
}

function obtenerClientes() {
    console.log('Descargando...');
    setTimeout(function() {
        console.log('Completo');
    }, 3000)
}
obtenerClientes();
