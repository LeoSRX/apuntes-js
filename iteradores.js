//For Loops (Ciclo Repetitivo)

for(let i = 0; i < 10; i++ ) {// Inicialización de variable-Cantidad de repeticiones-Incremento
  
    if(i % 2 == 0) {
        console.log(`El número ${i} es par.`); 
    } else {
        console.log(`El número ${i} es impar.`);
    }
}

for(let i = 0; i < 10; i++) {
    if(i == 7) {
        console.log(`Este número es el ${i}`);
        continue;   //Termina la ejecución de las sentencias de la iteración actual del bucle actual
    }
    console.log(i);
}

for(let i = 0; i < 10; i++) {
    if(i == 7) {
        console.log(`Este número es el ${i}`);
        break;      //Termina el bucle actual, sentecia switch o label
    }
    console.log(i);
}

const arrayProductos = ['Camisa', 'Boleto', 'Guitarra', 'Disco']; 
for(i = 0; i < arrayProductos.length; i++) {           //Recorre tantas veces como elementos tenga el Array
    console.log(`Tu producto ${arrayProductos[i]} fue agregado.`); //imprime el elemento de la posición exacta del Array
}


//While (Hacer mientras), primero confirma la condición y luego ejecuta

//let i = 0; seria lo correcto pero i ya está inicializado en la linea 29.
i = 0;

while(i < 10) {
    if(i == 5) {
        console.log('Cinco');
        i++; //continue cortaria la ejecución de este ciclo del while aquí, y nunca incrementaria la linea 46
        continue;
       
    }
    console.log(`Numero: ${i}`);
    i++;    //While siempre lleva el incremento al final
}

/* DO While (Hacer hasta), Ejecuta una vez el loop y 
luego verifica si cumple la condición,
repite el loop hasta cumplir la condición*/

//let i = 0;
i = 0; //Inicialización del contador
do {
    console.log(`Numero: ${i}`);
    i++;
} while(i<10);

// FOR EACH ejecuta la función indicada una vez por cada elemento del array.

const pendientes = ['Tarea', 'Comer', 'Proyecto', 'Aprender JavaScript']; //Array con nombre plural
pendientes.forEach(function(pendiente, index) {   //dentro del function recomiendo el nombre del Array en singular
    console.log(`${index}: ${pendiente}`);        //index devuelve la posición del indice en el que se encuentra pendiente
});

console.log(pendientes);

//MAP para recorrer un ARRAY
//El método map() crea un nuevo array con los resultados de la llamada a la función indicada aplicados a cada uno de sus elementos.
const carrito = [
    {id: 1, producto: 'Libro'},
    {id: 2, producto: 'Camisa'},
    {id: 3, producto: 'Guitarra'},
    {id: 4, producto: 'Disco'}
]
const nombreProducto = carrito.map(function(carrito) { //creó un nuevo array
    return carrito.producto + 's';                  //para volver plurales los productos.
})
console.log(nombreProducto);



const automovil = {
    modelo: 'Camaro',
    motor: 6.1,
    año: 1969,
    marca: 'Chevrolet'
}
//La instrucción for-in itera sobre todas las propiedades enumerables de un objeto
for(let auto in automovil) {   //Recorre el objeto automovil
    console.log(`${auto} : ${automovil[auto]}`);
}


//

const ciudades = ['Londres', 'Madrid', 'New York', 'París'];
const ordenes = new Set([123, 231, 131, 102]); //El objeto Set permite almacenar valores únicos de cualquier tipo
const datos = new Map(); //El objeto Map almacena pares clave/valor.
datos.set('nombre', 'juan');
datos.set('profesion', 'desarrolador web');

//Entries iterador, devuelve atributo y llave

for(let entrada of ciudades.entries() ) { //Entries de las ciudades
    console.log(entrada);
}

for(let entradas of ordenes.entries()) {
    console.log(entradas);
}

for (let entrada of datos.entries() ) {
    console.log(entrada);
}

//Values iterador, devuelve solo el valor

for(let entrada of ciudades.values ()) {
    console.log(entrada);
}

for(let entradas of ordenes.values  ()) {
    console.log(entradas);
}

for (let entrada of datos.values() ) {
    console.log(entrada);
}

//Keys iterador, devuelve los atributos

for(let entrada of ciudades.keys ()) {
    console.log(entrada);
}

for(let entradas of ordenes.keys  ()) {
    console.log(entradas);
}

for (let entrada of datos.keys() ) {
    console.log(entrada);
}

//por default

for(let entrada of ciudades) {  // imprime los valores
    console.log(entrada);
}

for(let entradas of ordenes) { // imprime los valores
    console.log(entradas);
}

for (let entrada of datos) { // devuelve todo en un arreglo como si fuera .entries()
    console.log(entrada);
}

//Iteradores para String

const mensaje = 'Aprendiendo JavaScript';
for (let letra of mensaje) {
    console.log(letra);
}

//NodeList

let enlaces = document.getElementsByTagName('a');
for (let enlace of enlaces) {
    console.log(enlace); //Imprime los a del html
}

/*let*/enlaces = document.getElementsByTagName('a');
for (let enlace of enlaces) {
    console.log(enlace.href); //Imprime solo los href
}

