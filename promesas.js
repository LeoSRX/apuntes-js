const ExamplePromise = new Promise(function(resolve, reject) { //esa function es la funcion ejecutora
    console.log('foo'); // al definir esta funcion se ejecuta
});
console.log('bar');
/* Una promesa tiene 3 estados
    -1: Pendiente... aun se esté ejecutando
    -2: Fullfilled... se ejecutó correctamente, para esto se utiliza resolve
    -3: Rejected... la promesa falló en su ejecución, para esto se utiliza reject*/

const promise1 = new Promise(function(resolve, reject) {
    setTimeout(function() {
        console.log('timeout');
        resolve('foo'); // podemos recuperar el valor foo cuando termine de ejecutarse este codigo con 2 metodos: then y catch
    }, 1000);
});

promise1.then(value => console.log(value)); // .then es para recuperar el valor de una promesa que se ejecutó correctamente (estado Fullfilled)
// los metodos .then se ejecutan cuando termina de ejecutarse el script principal

const promise2 = new Promise(function(resolve, reject) {
    setTimeout(function() {
        console.log('timeout');
        resolve('foo');
    }, 1000);
}).then(function(value) { // es mas habitual concatenar el .then
    console.log(`${value}  1`);
});

/* Metodo Reject*/

const promise3 = new Promise(function(resolve, reject) {
    setTimeout(function() {
        reject(new Error('an error ocurred!!'));
    }, 1000);
}).catch(function(value) {
    console.log(value.message);
});

/* Finally */

new Promise((resolve, reject) => {
        resolve(2 + Foo); //Foo no está definida, lo cual da un error, asi que ejecuta el metodo reject
        reject('error');
    })
    .then(value => console.log(value))
    .catch(value => console.log(value.message))
    .finally(() => console.log('Finally!')); 