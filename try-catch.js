function test(num1, num2) {
    setTimeout(function() {
        try {
        let a = num1;
        let b = num2;

        if(isNaN(a) || isNaN(b)) {
            throw 'Numero invalido';
        }
        alert(`El resultado es: ${a + b}`);
    } catch(error) {
        alert(`Error: ${error}`);
    }
    }, 1000)
    
}

test(1,2);