const person = { // declararlo con const no previene que se puedan modificar sus propiedades
    name: 'Leonel',
    surname: 'Robledo'
};

delete person.surname; //Elimina la propiedad del objeto
console.log(person.surname); // output: undefined

person.surname = 'Soria'; //Al no existir esta propiedad dentro del objeto, se crea.
console.log(person.surname);
const friend = {
    name: 'Leonel'
};

console.log(person === friend); // false
//A pesar de que tengan las mismas propiedades y los mismos valores para ellas, son distintas instancias

/*************************************************************/

person.name = 'Jorge'; // Cambiar el valor de una propiedad de un objeto
console.log(person.name);

const personFreezed = {
    name: 'Tomas'
};

Object.freeze(personFreezed); //el objeto se vuelve Inmutable
person.name = 'Leandro';
console.log(personFreezed.name); // output: Tomas

delete personFreezed.name; //Elimina la propiedad del objeto, pero al ser inmutable no la elimina


personFreezed.surname = 'Soria'; //Al no existir esta propiedad dentro del objeto, se crea, pero al ser inmutable no la crea
console.log(personFreezed);

/************** CLONACION DE OBJETOS *************************/

const person1 = {
    name: 'Carlos',
    adress: {
        street: 'Wolven street',
        number: 20
    }
};

const clonedPerson1 = {...person1 }; //Clona el objeto desestructurandolo // Shallow copy
//Shallow copy (copiado superficial) es una copia de bits de un objeto; esto quiere decir que se crea un nuevo objeto que tiene una copia exacta de los valores en el objeto original; PERO si alguno de los campos del objeto son objetos, solo se van a copiar las referencias a las mismas.
console.log(clonedPerson1 === person1); // false

delete person1.adress.number; // al ser ClonedPerson1 una Shallow copy, el objeto adress de person1 y clonedPerson1 hacen referencia al mismo objeto, asi que al eliminarlo en person1 tambien se elimina en clonedPerson1
console.log(person1);
console.log(clonedPerson1)

/********************DEEP CLONE***********************************/
const deepPerson = {
    name: 'Leo',
    adress: {
        street: 'Master street',
        number: 22
    }
};

const deepClonedPerson = JSON.parse(JSON.stringify(deepPerson)); //poco optimo, mejor usar lodash para hacer un mejor Deep Clone
delete deepPerson.adress.street;
console.log(deepPerson);
console.log(deepClonedPerson);

const person2 = {
    name: 'Matias',
};
person2.name = 'Raul';

const clonedPerson2 = person2; //Al modificar la propiedad en la linea anterior, tambien se modificará en clonedPerson2 
console.log(clonedPerson2 === person2); //output: true // Hacen referencia al mismo objeto

/*****************Metodos en Objetos********************/

const methodObject = {
    name: 'Neptune',
    surname: 'Korn',
    printName() { // Las funciones dentro de objetos se llaman METODOS
        console.log(this.name); //this para hacer referencia dentro del objeto
    }
}
methodObject.printName();

/******************Desestructurar objetos**************************/
// Permite extraer propiedades de los objetos directamente
const dPerson = {
    name: 'Leo',
    surname: 'Soria',
    adress: {
        street: 'Hawk Street',
        number: 22,
    }
}
const { surname, adress: { street } } = dPerson;
console.log(surname);
console.log(street);

const { adress: { street: adressStreet } } = dPerson; //podemos renombrar la variable que contendrá la propiedad del objeto
console.log(adressStreet);

/************Eliminar propiedades de objetos con Object Destructuring*********************/

const dPerson1 = {
    name: 'Leo',
    surname: 'Soria',
    adress: {
        street: 'Hawk Street',
        number: 22,
    },
    printAdress() {
        console.log(this.adress);
    }
};
dPerson1.printAdress();

const { printAdress, ...dPerson1NoMethod } = dPerson1; //crea una variable dPerson1NoMethod sin el metodo printAdress.
console.log(dPerson1);
console.log(dPerson1NoMethod);

/*********Obtener valores por defecto de un objeto con Object Destructuring****************/
const defaultPerson = {
    name: 'Leo',
    //lastName: 'Robledo',
    adress: {
        street: 'Hawk Street',
        number: 22,
    },
    printAdress() {
        console.log(this.adress);
    }
};
const { firstName, lastName = 'Soria' } = defaultPerson; // Asigna un valor por defecto a la variable en caso de que la propiedad no exista. Si existe, el valor por defecto no se asigna
console.log(lastName);

/***** Desestructuración y argumentos de funciones ******/

const fPerson = {
    name: 'Leonel',
    lastName: 'Robledo',
    adress: {
        street: 'Hawk Street',
        number: 22,
    },
    printAdress() {
        console.log(this.adress);
    }
};

function printPersonName({ name }) {
    console.log(name);
};
printPersonName(fPerson);

/* Prototipos */
// Todos los objetos tienen un prototipo, es como un molde que nos permite tener determinadas propiedades

const pPerson = {
    user: 'LeoSRX',
    mail: 'leusoria@gmail.com',
    location: {
        continent: 'South America',
        country: 'Argentina',
    },
};
console.log(pPerson.toString()); //pPerson no tiene un metodo toString(), pero si su prototipo