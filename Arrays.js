//Crear Array

const numeros = [10, 20, 30, 40, 50];
console.log(numeros);

//Arreglo Mezclado
const mezclado = ['Hola', 20, true, null, false, undefined];
console.log(mezclado);

//Array de Strings (metodo alternativo)
const meses = new Array('Enero', 'Febrero', 'Marzo', 'Abril');
console.log(meses);
console.log(Array.isArray(meses)); //Comprueba si es un Array
console.log(meses[3]); //Imprime el contenido de la posición del array determinada por el indice

//Añadir en un Array
meses.unshift('Mes 0'); //Agrega una nueva posición del array al principio de este
meses[4] = 'Mayo'; //Crea o modifica la posición del array determinada por el indice
console.log(meses[4]);
meses.push('Junio'); //Agrega una nueva posición al array al final de este

//Cambiar un elemento del Array
const numer = [1, 2, 3];
numer[0] = 4; // Cambia el valor de la posición del indice [x]
console.log(numer);

//Eliminar en un Array
meses.pop(); //Elimina el ultimo elemento del Array
meses.shift(); //Elimina el primer elemento del array
meses.splice(2, 1); //Elimina el elemento seleccionado en el primer parametro, el segundo parametro indica cuantos elementos a partir de esa posición desea eliminar
console.log(meses);

//Revierte las posiciones del Array
const mesesR = meses;
mesesR.reverse();
console.log(mesesR);

//Encontrar un elemento en el Array
console.log(meses.indexOf('Abril')); //Muestra, si es que existe, la posición en la que se encuentra el elemento

//Unir un Array con otro
let array1 = [1, 2, 3],
    array2 = [9, 8, 7];
console.log(array1.concat(array2));

//Ordenar un Array

const frutas = ['Banana', 'Kiwi', 'Manzana', 'Frutilla', 'Naranja'];
frutas.sort(); //Ordena alfabeticamente el Array A-z
console.log(frutas);

const arrayNum = [3, 9, 76, 23, 25, 61, 666, 23.1, 23.9];
arrayNum.sort(); //Ordena los numeros guiandose del primer digito, No es la Forma Correcta
console.log(arrayNum);

const arrayNum1 = [3, 9, 76, 23, 25, 61, 666, 23.1, 23.9];
arrayNum1.sort(function(a, b) {
    return a - b; //Forma correcta de ordenar numeros
}); // de mayor a menor en un array
console.log(arrayNum1);


const arrayNum2 = [3, 9, 76, 23, 25, 61, 666, 23.1, 23.9];
arrayNum2.sort(function(a, b) {
    return b - a; //Ordenar de mayor a menor
});
console.log(arrayNum2);



const a = {};
const b = {};

console.log(a === b);

//.reduce principalmente para transformar los datos en un solo valor
const reducido = [1, 2].reduce((acc, el) => acc + el, 0);


const numeros = [1, 2, 3, 4, 5];
const resultado = numeros.reduce((acc, el) => acc + el, 5);

const mascotas = [
    {nombre: 'Pelusa', edad: 8, tipo: 'gato'},
    {nombre: 'Sylvanas', edad: 1, tipo: 'gato'},
    {nombre: 'Sebas', edad: 3, tipo: 'perro'},
    {nombre: 'Snow', edad: 2, tipo: 'conejo'},
]

const indexed = mascotas.reduce((acc, el) => ({
    ...acc,
    [el.nombre]:el,
}), {});

console.log(indexed['Sylvanas']);

const anidado = [1,[2, 3], 4, [5]];
const plano = anidado.reduce((acc, el) => acc.concat(el), []);

// Filter

const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

const result = words.filter(word => word.length > 6); // crea un nuevo array con todos los elementos que cumplan la condicion

console.log(result);
