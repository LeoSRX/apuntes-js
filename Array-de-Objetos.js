/*Crear Objeto, se usan llaves para crear objetos
puede ser de tipo CONST o tipo LET*/

const persona = {
    nombre: 'Leonel', //nombre, apellido, profesion son Atributos
    apellido: 'Soria', //tambien los llaman llaves
    profesion: 'Programador',
    email: 'leusoria@gmail.com',
    edad: 24,
    telefono: 3834963201,
    musica: ['Rock', 'Trash', 'K-pop', 'Jazz'],
    hogar: {
        pais: 'Argentina',
        provincia: 'Catamarca',
        departamento: 'Valle Viejo'
    },
    nacimiento: function() { //Las funciones dentro de objetos se llaman METODOS
        return new Date().getFullYear() - this.edad; //regresa el valor resultante del año en el que estamos - edad
    }
}
console.log(persona);

/*En Arrays accedes a un elemento utilizando el indice, En Objetos accedes utilizando atributos de la siguiente manera*/
console.log(persona.nombre);
console.log(persona.profesion);
console.log(persona.edad);
console.log(persona.telefono);
console.log(persona.musica);
console.log(persona.musica[2]);
console.log(persona.nacimiento());

//Array de Objetos

const autos = [
    { modelo: 'Mustang', motor: 6.2 },
    { modelo: 'Camaro', motor: 6.1 },
    { modelo: 'Challenger', motor: 6.3 }
];
console.log(autos);
console.log(autos.length);
console.log(autos[0]);
console.log(autos[1].modelo);

autos[2].modelo = 'Spider'; //Cambia el contenido del atributo especifico 
console.log(autos);

//Con for

const camioneta = [
    { modelo: 'Ranger', motor: 8.2, cantAsiento: 4 },
    { modelo: 'S10', motor: 8.1, cantAsiento: 5 },
    { modelo: 'Hilux', motor: 8.3, cantAsiento: 6 }
];

for (let i = 0; i < camioneta.length; i++) {
    console.log(camioneta[i]);
    console.log(camioneta[i].modelo);
    console.log(camioneta[i].motor);
    console.log(camioneta[i].cantAsiento);


}

console.log(camioneta);
console.log(camioneta.length);

const moto = [
    { modelo: 'Cripton', motor: 110 },
    { modelo: 'CG', motor: 125 },
    { modelo: 'Tornado', motor: 250 }
];

for (let i = 0; i < moto.length; i++) {
    console.log(`${moto[i].modelo} ${moto[i].motor}`); // ${} muestra el contenido en forma de 
}                                                      // string