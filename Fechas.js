// En JavaScript existe un objeto llamado Date
// Date utiliza GET para obtener un valor y SET para agregarlo o sobreescribirlo
const diaHoy = new Date();
console.log(diaHoy);

//Fecha en especifico, Mes-Dia-Año
let navidad2020 = new Date('12-25-2020');
console.log(navidad2020);

//MES
let valor;
valor = diaHoy.getMonth(); //La posición de los meses inicia en 0, Octubre es 9 por ej.
console.log(valor);

//DIA
let valorD;
valorD = diaHoy.getDate();
console.log(valorD);

//AÑO
let valorA;
valorA = diaHoy.getFullYear();
console.log(valorA);

//HORA
let valorH;
valorH = diaHoy.getHours();
console.log(valorH);

//Minutos
let valorMinutos;
valorMinutos = diaHoy.getMinutes();
console.log(valorMinutos);

//Milisegundos desde el año 1970
let valor1970;
valor1970 = diaHoy.getTime();
console.log(valor1970);

//Modificar una fecha
let valorEdit;
valorEdit = diaHoy.setFullYear(2021); //usamos Set para cambiar el valor de Date
valorEdit = diaHoy.getFullYear();
console.log(valorEdit);
