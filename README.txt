La mayoria de archivos son para repaso o ejemplos

Los archivos bien explicados para estudio son:
    *async-await.js
    *Array-de-Objetos.js
    *Conversiones de tipo.js
    *Fechas.js
    *funciones.js
    *IfElseSwitch.js
    *objetos.js
    *operadoresDeComparacion.js
    *promesas.js
    *promesas2.js
    *this-arrowFunctions.js


Para usar los ejemplos reemplaza el valor de src en index.html (linea 17)
    por el archivo que quieras utilizar:

    <script src="test.js" defer></script>

r