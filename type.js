let valor1, dateValue;

//Tipo Number
valor1 = 20; 
//Tipo Cadena
valor1 = 'Cadena de Texto';
valor1 = '20';
//Tipo Booleano
valor1 = true;
valor1 = false;
//Tipo Objeto
valor2 = [1,2,3,4,5];
valor1 = null;
valor1 = undefined;
valor1 = {
    nombre: 'Juan',
    profesion: 'Desarrollador Web'
};
//Fecha
dateValue = new Date();

console.log(dateValue);
console.log(typeof dateValue);

console.log(typeof valor1);
console.log(typeof valor2);

let valor;

valor = 20.2;

//Saber el tipo de valor
console.log(typeof valor);