/* new Promise(function(resolve, reject) {
    setTimeout(() => resolve('promises'), 1000);
}).then(function(result) {
    return `${result} are`;
}).then(function(result) {
    return `${result} awesome.`;
}).then(function(result) {
    console.log(result); // 'promises are awesome'
})

//tambien podemos hacer 

const promise = new Promise(function(resolve, reject) {
    setTimeout(() => resolve('promises'), 1000);
})

//Ejemplo 1: Concatenando los then

promise.then(function(result) { //el metodo then siempre devuelve un objeto promise
    return `${result} are`;
}).then(function(result) {
    return `${result} awesome.`;
}).then(function(result) {
    console.log(result); // 'promises are awesome'
})

//Ejemplo 2: Adjuntarle distintos then al mismo objeto promise, le proporciono 3 callbacks diferentes

promise.then(function(result) {
    return `${result} are`; // 'promises are'
});
promise.then(function(result) {
    return `${result} awesome`; // 'promises awesome'
});
promise.then(function(result) {
    console.log(result) // 'promises'
});

// Ejemplo 3: dentro de un then podemos devolver una nueva promesa para concatenar la ejecucion de multiples promesas, el tiempo de ejecucion total serian 3 segundos

const promise3 = new Promise(function (resolve, reject) {
    setTimeout( () => resolve('promises'), 1000);
});

promise3.then(function(result) {
    return new Promise(function (resolve, reject) {
        setTimeout(() => resolve(`${result} are`), 2000);
    });
})
.then(function(result) {
    return `${result} awesome`;
})
.then(function (result) {
    console.log(result); // 'promises are awesome
});


// FETCH

fetch('https://reqres.in/api/users?page=2') //fetch genera una promesa
    .then(function(response) {
        return response.json(); //este metodo .json() nos permite convertir a objeto, es otra promesa
    })
    .then(function(json) {
        console.log(json);
    });


    //CATCH

    const promiseCatch = new Promise(function(resolve, reject) {
        setTimeout(() => {
            reject('an error ocurred')
        }, 1000);
    });
    promise.catch(error => console.log(error));

    //Ejemplo 1:
    fetch ('https://api.com/me')
        .then((response) => response.json())
        .then((user) => fetch(`https://api.com/users/${user.id}/articles`))
        .then((response) => response.json())
        .catch((error) => console.log(`Error: ${error.message}`)); //Failed to fetch

//JS trata de ejecutar siempre el catch mas cercano a donde se produce el error

fetch('https:badresponse.com')
    .then(response => response.json())
    .catch(error => {return { id:1, fullname: 'default user'};}) //devuelve un objeto por defecto
    //catch tambien nos permite capturar un error y devolver un valor valido para que se siga ejecutando la cadena  de promesas
    .then(user => console.log(user.fullname)); // 'default user'

//Ejemplo 2: 
fetch ('https://api.com')
    .catch(error => {
        console.log(error);
        throw new Error('El fetch fallo'); // lo vuelve a lanzar y JS busca el siguiente Catch ya que entiende que se produjo un nuevo error
})
    .then(response => response.json())
    .then(user => console.log(user.fullname))
    .catch(error => {
        alert(error);
    })

// Ejemplo 3: Promesas en paralelo
// Los siguientes metodos devuelven un array de promseas

const promesasParalelas = [
    new Promise((resolve) => resolve('Primera')),
    new Promise((resolve, reject) => reject('Segunda')),
    new Promise((resolve) => resolve('Tercera')),
];
Promise 
    .all(promesasParalelas) //Si todas las promesas se ejecutan se corre el siguiente then
                            //Si falla, se ejecuta el catch con la promesa que fallo
    .then(result => console.log(result)) // ['Primera', 'Segunda', 'Tercera']
    .catch((error => console.log(error))); // Se ejecuta el catch con el error lanzado por
                                           // la primera promsea que fallo en su ejecucion

// Promise.race: Nos permite ejecutar varias promesas a la vez y obtener el resultado de la primera promesa que se resuelva o se rechace

//Ejemplo 1:
const promisesRace = [
    new Promise((resolve, reject) => reject('Primera')),
    new Promise((resolve) => resolve('Segunda')),
    new Promise((resolve) => resolve('Tercera'))
];
Promise
    .race(promisesRace)
    .then(result => console.log(result))
    .catch(error => console.log(`Catch: ${error}`)) */

// Promise.allSettled recibe como argumento un array de promseas
// en .then devuelve un array con el resultado de cada promesa y el estado en el que termina

// Nos permite obtener el estado de las promesas independientemente de si fallaron o no
const promisesAS = [
    new Promise((resolve, reject) => reject('Primera')),
    new Promise((resolve, reject) => reject('Segunda')),
    new Promise((resolve) => resolve('Tercera'))
];

Promise
    .allSettled(promisesAS)
    .then(result => console.log(result));

// Promise.any: recibe un array de promesas y se resuelve tan pronto como una de las promesas pasadas como argumentos pase al estado fullfilled, solo lanza una excepcion si todas fallan

const promisesAny = [
    new Promise((resolve, reject) => reject('Primera')),
    new Promise((resolve) => resolve('Segunda')),
    new Promise((resolve) => resolve('Tercera'))
];
Promise
    .any(promisesAny)
    .then(result => console.log(result))
    .catch(error => console.log(error)) // solo ejecuta .catch si todas las promesas fallan

