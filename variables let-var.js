
let nombre, mensaje;

nombre= 'Leonel';
mensaje= 'Y entonces dije \'Buen Curso!\' ';

//Concatenar
mensaje = 'Java' + 'Script';
console.log(mensaje);

//Uniendo variables en una sola
let aprendiendo= 'Aprendiendo',
    tecnologia= 'JavaScript';

console.log(aprendiendo.concat(' ', 'es Genial'));

//Hacer mayusculas el contenido de una variable

console.log(aprendiendo.toUpperCase() );

//Hacer minúsculas el contenido de una variable

console.log(tecnologia.toLowerCase() );

/*Encontrar una palabra, la consola muestra el numero de orden de cada caracter
 en el que comienza la palabra, si devuelve -1 la palabra no existe*/

let mensajeLargo = 'Aprendiendo JavaScript, CSS, HTML para hacer Front End'
console.log(mensajeLargo.indexOf('JavaScript'));

//Toma como intervalo el orden de caracteres de la variable, en este caso de 0 a 10
console.log(mensajeLargo.substring(0,10));

//Toma desde el ultimo caracter hasta el primero, segun la cantidad indicada
console.log(mensajeLargo.slice(-20));

//Toma desde el primer caracter hasta el ultimo, segun la cantidad indicada
console.log(mensajeLargo.slice(20));

//Toma desde el caracter señalado hasta el segundo caracter señalado
console.log(mensajeLargo.slice(10,20));

//Regresa un array separando caracteres según el contenido seleccionado de split
console.log(mensajeLargo.split(' '));

//Busca la primera cadena de caracteres y la reemplaza por la segunda
console.log(mensajeLargo.replace('CSS', 'PHP'));

//Regresa true o false dependiendo de la existencia del contenido
console.log(mensajeLargo.includes('CSS'));

//Repite el contenido la cantidad de veces indicada
console.log(tecnologia.repeat(10));
