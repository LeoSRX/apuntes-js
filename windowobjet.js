
//Window Objet

/*
//Prompt
const nombre = prompt('Ingrese su nombre');
console.log(`Bienvenido ${nombre}`);

//Confirm
if(confirm('Eliminar?')) {
    console.log('Eliminado');
} else {
    console.log('Nada pasó');
}
*/

let alto,
    ancho;

altura = window.outerHeight; //outer muestra el tamaño de todo el navegador
ancho = window.outerWidth;

console.log(altura);
console.log(ancho);

altura = window.innerHeight; //inner muestra el tamaño de la ventana del navegador sin la interfaz
ancho = window.innerWidth;

console.log(altura);
console.log(ancho);

//Ubicación

let ubicacion;
ubicacion = window.location;
console.log(ubicacion);

/*Redireccionar a través de JavaScript

window.location.href = 'http://twitter.com'
*/

//Navegador
ubicacion = window.navigator;
console.log(ubicacion);
ubicacion = window.navigator.appName;
console.log(ubicacion);
ubicacion = window.navigator.appVersion;
console.log(ubicacion);
ubicacion = window.navigator.userAgent;
console.log(ubicacion);

ubicacion = window.navigator.language;
console.log(ubicacion);