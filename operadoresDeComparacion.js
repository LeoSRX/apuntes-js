//Operadores de Comparación

const numero1 = 20;
const numero2 = 50;
const numero3 = '20';

console.log( numero1 > numero2); //Mayor
console.log( numero1 < numero2); //Menor

console.log( 20 == '20'); //Igual

console.log(20 === '20'); //Comparador mas estricto, Compara el contenido y tambien el tipo de dato

console.log(20 != 30); //Distinto de...

console.log('hola' == 'hola '); //Igual contenido de cadena

console.log('a' > 'b'); //JS considera A la letra de menor rango y z la letra de Mayor rango

