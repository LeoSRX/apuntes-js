const   numero1 = '50',
        numero2 = 10.5, 
        numero3 = 'tres';


// Convertir Strings a Numbers
console.log(Number(numero1) + numero2);
console.log(typeof parseInt(numero1));//Convertir especificamente a entero.
console.log(parseFloat(numero2));//Convertir especificamente a flotante (decimal).
console.log(typeof Number(numero3)); //cambia el tipo de variable pero al quedar tipo Number no sabe leer a 'tres'

let dato;

dato = Number('20');
dato = Number('20.32');
dato = Number(true);
dato = Number(false);
dato = Number(null);
dato = Number(undefined);//NaN

//ToFixed

dato = 123124.1234152;
console.log(dato.toFixed(3)); //Determina la cantidad de numeros a mostrar de la parte decimal

console.log(typeof dato);

dato1 = '124987.123697';
console.log(parseFloat(dato1).toFixed(3));//Convierte de string a flotante con 3 numeros despues de la coma

dato2 = '91728.19827';
console.log(parseInt(dato2).toFixed(2));//Convierte de string a entero mostrando 2 ceros despues de la coma


//Convertir de Numbers a Strings

let ad;
ad = 90210;
console.log(ad.length); //length solo funciona con variables de tipo String
console.log(typeof ad);

cp = 90210;
cp = String(cp);
console.log(cp.length);

//Booleanos

datoB = true;
datoB = false;

datoB = String(datoB);

console.log(datoB);
console.log(typeof datoB);
console.log(datoB.length);

//Array a String

datoArray = String([1,2,3]);                 //se pueden 

datoArray = ([1,2,3]);                       //usar ambas
datoArray = datoArray.toString();

console.log(datoArray);
console.log(typeof datoArray);
console.log(datoArray.length);

//Null y Undefined no pueden ser convertidos a String