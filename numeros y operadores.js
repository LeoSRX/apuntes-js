//Numeros en JS

const numero1 = 30;
const numero2 = 20;
const numero3 = 20.490;
const numero4 = .1020;
const numero5 = -3;
const numero6 = 36;


let resultado, resultadoM, resultadoPi, resultadoRed, resultadoRedDown, resultadoRedUp, resultadoPot, resultadoRaiz2, resultadoAbs;

//operaciones artitmeticos
resultado = numero1 + numero2;
resultado = numero1 - numero3;
resultado = numero2 * numero4;
resultado = numero1 / numero5;
//MODULO, el resto de la división entre dos numeros
resultadom = numero1 % numero2;
//Pi
resultadoPi = Math.PI;
//Redondeo
resultadoRed = Math.round(numero3);
//Redondeo hacia arriba
resultadoRedUp = Math.ceil(numero3);
//Redondeo hacia abajo
resultadoRedDown = Math.floor(numero3);
//Raiz cuadrada
resultadoRaiz2 = Math.sqrt(numero6);
//Potencia: primero la base y luego el exponente
resultadoPot = Math.pow(numero5, 3);
//Absoluto
resultadoAbs = Math.abs(numero5);
//Numero minimo
resultadoMin = Math.min(numero3, numero6, 25, 43);
//Numero maximo
resultadoMax = Math.max(numero3, numero6, 25, 43);
//Numero Aleatorio menor a 1
resultadoAlt = Math.random();

console.log(resultadoAlt);
console.log(resultadoMax);
console.log(resultadoMin);
console.log(resultadoPot);
console.log(resultadoAbs);
console.log(resultadoRaiz2);
console.log(resultadoRedDown);
console.log(resultadoRedUp);
console.log(resultadoRed);
console.log(resultadoPi);
console.log(resultadom);
console.log(resultado);