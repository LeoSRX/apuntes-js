// Estructuras de control

//IF ELSE

const edad = 17;

if(edad >= 18) {                    //Si cumple la condición ejecuta lo que está dentro.
    console.log('Acceso permitido');
} else {                              //Si NO cumple la condición, ejecuta 
    console.log('Acceso denegado'); //lo que está dentro del Else.
}

//Comprobar que una variable tiene un valor

let puntaje = 1000;
if(puntaje) {
    console.log(`El puntaje fue de ${puntaje}`);
} else {
    console.log(`No hay puntaje`);
}


let efectivo = 500;
let totalCarrito = 300;

if(efectivo > totalCarrito) {
    console.log('Pago Correcto');
} else {
    console.log('Fondos insuficientes');
}

//ELSE IF

//Operador AND: && es válido al cumplir todas las condiciones

let hora = 22;
if(hora > 0 && hora <= 10) {
    console.log('Buenos Dias')
} else if(hora >10 && hora<=18) {
    console.log('Buenas Tardes')
} else if(hora >18 && hora<=24) {
    console.log('Buenas Noches')
} else {
    console.log('Hora no válida');
}

//Operador OR: || Es válido con cumplir aunque sea una condición

let cash = 300,
    credito = 300,
    disponible = cash + credito,
    carrito = 500;
if(carrito < cash || carrito < credito || carrito < disponible) {
    console.log('Puede pagar');
} else {
    console.log('No puede pagar');
}

//Ternario
const logeado = true;
console.log(logeado === true ? 'Si se logeó ' : 'No se logeó');
/*  primero la condición,
    ? represente el if
    : representa le else  */

// Switch
const metodoPago = 'tarjeta';

switch(metodoPago) {                               //ejecuta cada case hasta que la condición se cumple
    case 'efectivo':                               //en ese caso se detiene el switch
        console.log(`El usuario pagó con ${metodoPago}`);
        break;
    case 'cheque':
        console.log(`El usuario pagó con ${metodoPago}`);
        break;
    case 'tarjeta':
        console.log(`El usuario pagó con ${metodoPago}`);
        break;
    default:                                        //En caso de no cumplirse ningun case, se ejecuta el default
        console.log('Metodo de pago no soportado');
        break;
}

const fecha = new Date();
console.log(fecha.getMonth());

let mes;
switch(new Date().getMonth()) { //getMonth utiliza el valor 0 para Enero y 11 para Diciembre
    case 0: 
        mes = 'Enero';
        break;
    case 1: 
        mes = 'Febrero';
        break;
    case 2: 
        mes = 'Marzo';
        break;
    case 3: 
        mes = 'Abril';
        break;
    case 4: 
        mes = 'Mayo';
        break;
    case 5: 
        mes = 'Junio';
        break;
    case 6: 
        mes = 'Julio';
        break;
    case 7: 
        mes = 'Agosto';
        break;
    case 8: 
        mes = 'Septiembre';
        break;
    case 9: 
        mes = 'Octubre';
        break;
    case 10: 
        mes = 'Noviembre';
        break;
    case 11: 
        mes = 'Diciembre';
        break;
}
console.log(`Este mes es ${mes}`);