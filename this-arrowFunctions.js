/* THIS */
// Hay que tener en cuenta el entorno de ejecución
console.log(window);
// por defecto la variable this representa el objeto windows

console.log(this === window); // true

// el valor de la variable this esta asociado al objeto que invoco la funcion

function foo() {
    console.log(this); //
}
foo(); // el entorno que esta ejecutando la funcion foo() es el objeto window
window.foo();

function Person(name, email) {
    this.name = name;
    this.email = email;
}

const person = Person('g', 'm');
console.log(person); // undefined
console.log(window. name); // g

// para evitar este comportamiento


function Person1(name, email) {
    this.name = name;
    this.email = email;
    // return this esta implicito
}

const person1 = new Person1('g', 'm'); // se comporta como constructor
console.log(person1); 

/* This dentro de metodos */

const person2 = {
    name: 'Leo',
    sayHello: function() {
        console.log(`Hola ${this.name}`);
    }
}
person2.sayHello(); 
// quien ejecuta sayHello() es el objeto person2, 
// por ende this es el propio objeto person2

const person3 = {
    fullname: 'Leo',
    sayHello: function() { //1* sayHello devuelve una funcion
        return function() { 
            console.log(`Hola ${this.fullname}`);
        }
    }
}
person3.sayHello(); // no muestra nada
person3.sayHello()(); // output: Hola
// 1* devuelve una funcion:
const func = person3.sayHello(); 
func(); // quien ejecuta esta funcion es el objeto windows, es lo mismo que window.func()

// para resolver esto, podemos forzar el valor de la variable this dentro de una funcion
const person4 = {
    fullname: 'Leo',
    sayHello: function() { //1* sayHello devuelve una funcion
        const devFunction =  function() { 
            console.log(`Hola ${this.fullname}`);
        };
        return devFunction.bind(this); //el valor actual de this en esta linea es la de person4
    }
}
const func1 = person4.sayHello();
func1();

/* Arrow Functions */

const sayHello = () => console.log('Hola a todos');
sayHello();


const myArray = [1, 2, 3, 4, 5];
const newArray =  myArray.map(value => value * 2);
console.log(newArray);

/* Declarar una funcion que devuelva un objeto */

const MyFuncObj = a => ({prop:a}); // al devolver un objeto debe ir entre parentecis
console.log(MyFuncObj(2));


/* This dentro de Arrow functions */

const personA = {
    name: 'Leonel',
    sayHello: function() {
        return () => console.log(`Hola ${this.name}`);
        // la arrow function no tiene en cuenta quien la invoca sino donde se esta invocando
        // la funcion esta definida dentro del contexto personA y hereda la variable this de la funcion que la rodea, sino, del objeto window
    }
}
personA.sayHello()();

/* Cuidado con esto: */

const personB = {
    name: 'Jorge',
    sayHello: () =>  {
        console.log(`Hola ${this.name}`) 
        //no funciona porque no hay una funcion que rodee a la arrow function, por ende 
        //el this que utiliza es el del objeto window
    }
    
}
personB.sayHello();