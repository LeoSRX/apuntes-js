const numeros = [1, 2, 3, 4, 5];
const resultado = numeros.reduce((acc, el) => acc + el, 0);

const mascotas = [
    {nombre: 'Pelusa', edad: 8, tipo: 'gato'},
    {nombre: 'Sylvanas', edad: 1, tipo: 'gato'},
    {nombre: 'Sebas', edad: 3, tipo: 'perro'},
    {nombre: 'Snow', edad: 2, tipo: 'conejo'},
]

const indexed = mascotas.reduce((acc, el) => ({
    ...acc,
    [el.nombre]:el,
}), {});

const numbers = [1, [2, 3], 4, [5]] //concatenar con .reduce



const plano = numbers.reduce((acc, el) => acc.concat(el), []);
console.log(plano);


console.log(indexed['Sylvanas']);
console.log(indexed['Pelusa']);




