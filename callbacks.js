// una funcion CallBack es una funcion que se pasa a otra funcion como argumento

/* function primero() {
    setTimeout(function() {
         console.log('primero');
    }, 1000);
}

function segundo() {
    console.log('segundo');
}
primero();
segundo(); */

//Ejemplo:

function primero(segundo) { // una callback es una manera de asegurarnos que un codigo no
                            // se ejecute hasta que otro codigo haya terminado de ejecutarse
    setTimeout(function() {
         console.log('primero');
         segundo();
    }, 1000);
}

function segundo() {
    console.log('segundo');
}
primero(segundo);


