// Async - Await es la forma mas habitual de trabajar con las promesas
// Await siempre va dentro de una funcion declarada con Async

/* function getUser() {
    return new Promise((resolve, reject) => {
        resolve({
            id: 1,
            name: 'Leonel',
            categoryId: 2
        })
    })
}

function getCategory() {
    return new Promise((resolve, reject) => {
        resolve({
            id: 2,
            name: 'Categoria'
        })
    })
}

getUser().then(function(user) {
    console.log(user);
    return getCategory().then(function (category) {
        return {
            ...user,
            category: category
        };
    });
})
.then(fullUser => console.log(fullUser)); */

//Para hacer lo mismo pero con la estructura async-await

async function getUser() { //async function indica que devuelve una promesa
    return new Promise((resolve, reject) => {
        resolve({
            id: 1,
            name: 'Leonel',
            categoryId: 2
        })
    })
}

async function getCategory() {
    return new Promise((resolve, reject) => {
        resolve({
            id: 2,
            name: 'Categoria'
        })
    })
}

async function getFullUser() { //para ejecutar un await, la funcion debe ser async
    const user = await getUser(); //como getUser devuelve una promesa, await indica 
                                  //que espere hasta que se resuelva
    const category = await getCategory();
    return console.log({
        ...user,
        category: category
    });
}
getFullUser();




