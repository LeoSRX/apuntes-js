document.addEventListener('DOMContentLoaded', function() {
    const logo = document.getElementById('logo'); //guardar un elemento segun su id
    console.log(logo); 

    logo.hasAttribute('class'); // preguntamos si la variable tiene un atributo llamado clase

logo.getAttribute('class'); // obtenemos el nombre de la clase que posee la variable logo

logo.setAttribute('class', 'nueva clase'); //establece el valor de un atributo en el elemento indicado, si el atributo ya existe, el valor es actualizado

logo.removeAttribute('name'); //Elimina el atributo seleccionado

const navegacion = document.getElementById('navegacion');

navegacion.style.display = 'none'; //Setea el display en lo que indicamos en el parametro


let nav = document.getElementsByClassName('navegacion'); // guarda todos los elementos con la clase del parametro, los devuelve en un array

let contenido = document.getElementsByClassName('contenido');

contenido[0].style.display = 'none'; //setea en none el atributo Display de la posicion 0 del array que contiene los elementos geteados anteriormente

let enlaces = document.getElementsByTagName('a'); // Almacena en la variable enlaces todos los elementos con la etiqueta enviada como parametro

for(let i = 0; i < enlaces.length; i++) {
    enlaces[i].setAttribute('target', '_blank');
}

let enlacesSideBar = document.getElementById('sidebar').getElementsByTagName('a');
// aqui almacenamos en la variable enlacesSideBar todos los elementos con la etiqueta 'a' que se encuentren dentro del elemento con id 'sidebar'
for(let i = 0; i < enlacesSideBar.length; i++) {
    enlacesSideBar[i].setAtrribute('href', )
}

 let logo = document.querySelector('#logo'); // # para los id
 let logo = document.querySelector('.logo'); // almacena el primer elemento que tenga la clase logo

 let encabezado = document.querySelector('aside h2'); //selecciona la etiqueta h2 dentro de la etiqueta aside

 let encabezado = document.querySelectorAll('h2'); // almacena todas las etiquetas h2 en el documento
 console.log(encabezado[0].innerText);

 // document.querySelectorAll tambien podemos enviarle mas parametros
 let encabezado = document.querySelectorAll('h2, footer p');
 // de esta manera almacenaria todos los elementos h2 y todos los p dentro de footer\

let enlaces = document.querySelectorAll('a');

let enlaces = document.querySelectorAll('#menu ul li a');
// almacena todos los links 'a' que esten dentro de los li que estan dentro de ul que estan dentro del elemento con el id menu
console.log(enlace[1].innerText); // para innerText tienes que seleccionar el elemento del array

console.log(enlaces.nodeType); 
// El valor del elemento de tipo nodo es 1

console.log(enlaces.nodeName); // regresa el nombre del nodo

console.log(enlaces.nodeAttributes); // muestra un array con los atributos 
});

